
##Darnet/YOLO optmized for Jetson

This software has being adapted from [this link](https://github.com/AlexeyAB/darknet) (Thanks for the authors) for educational purpose, aiming memory optimization of Darknet on Jetson.

It has being tested on Jetson Nano only, because this is the only GPU I have.

Additional implementation/evalution details are [here](https://etanba007.blogspot.com/2020/05/darknetyolo-optimized-for-jetson.html) ([詳細](https://tanbalabs.hatenablog.com/entry/2020/05/14/231029))

Now we can train and inference YOLOv3 on higher width/height. My Nano is configured in [headless mode](https://devtalk.nvidia.com/default/topic/1050739/how-to-boot-jetson-nano-in-text-mode-/) to have more free memory.
The training takes days even for 2 classes, but if you are limited on resources like me, you can train your own data set.


##Background

Darknet allocates memory for the same data twice: CPU and GPU.
However, in Jetson (Tegra), both the CPU and GPU [share the same DRAM memory](https://docs.nvidia.com/cuda/cuda-for-tegra-appnote/index.html). So, we do not need to allocate twice.

Another way to reduce memory usage is to use Mixed Precision. Although Mixed Precision uses more memory for Weight updates (needs a copy of both fp16 and 32), it decreases the total utilization factor.

I have adapted Darknet to use unified memory (and avoid memory duplication) as well as fp16 (mixed precision for training) to reduce memory usage.
The Mixed Precision implementation ([link](https://docs.nvidia.com/deeplearning/sdk/mixed-precision-training/index.html)) is based on TensorFlow implementation of [this link](http://on-demand.gputechconf.com/gtc-taiwan/2018/pdf/5-1_Internal%20Speaker_Michael%20Carilli_PDF%20For%20Sharing.pdf). I have also referenced [this](http://on-demand.gputechconf.com/gtc-taiwan/2018/pdf/5-1_Internal%20Speaker_Michael%20Carilli_PDF%20For%20Sharing.pdf) and [this](http://on-demand.gputechconf.com/gtc/2018/presentation/s8923-training-neural-networks-with-mixed-precision-theory-and-practice.pdf) for the implementation of dynamic loss scaling. The difference is the rule to increase the scale.

Can this code be used on other GPUs (Tested on Jetson Nano only)? I do believe it works on other Jetson with  Maxwell architecture. For Xavier, I have no means to verify if it uses the correct implementation of atomicAdd. For other dGPUs, it will not work because I have customized to use unified memory.

##Limitations

* Limited tests on Jetson Nano because I could allocate max 10H/W so far, and due to long training time.
* Although implementing fp16 and Mixed Precision, we will have no faster processing on Nano because it does not implement Tensor Cores.
* (Partially)Adapted layers: convolutional, route, shortcut, upsample, yolo, cost. (Works for standard cfg/yolov3.cfg, but not for tiny nor v4/v2, nor classifications.)
* Adapted only Leaky activation.
* Slow training.

##Wish list:

If I have time, I wish:

* Improve the code. Currently, this code is the first that worked. I had to do many tentatives, and I had no much time to clean up all of them.
* Implement more layers, activations, etc, at least for yolov2 and tinies.
* Do a better profiling of the training on an attempt to speed-up.
* Find Nano processes that uses huge amount of memory, and investigate if we can stop them.

##Additional Mixed Precision configuration items on [net] section of cfg file (Self explanatory)

|additional item          | Default value | Comments
|-------------------------|---------------|----------
|max_loss_scale           |16384.0        |
|min_loss_scale           | .0009765625   |
|initial_loss_scale       | 1.0           |
|loss_scale_inc_iterations| 1000          | Used after burn_in



More details of the algorithm for loss scale and results are 
 [here](https://etanba007.blogspot.com/2020/05/darknetyolo-optimized-for-jetson.html)

If you want to train on your own dataset, you will also need to adjust at least the anchor boxes. Details for these as well as other paramenters are [here](https://github.com/AlexeyAB/darknet)


##Makefile

There are two new configurations in Makefile:

    JETMEM: Set this to 1 to use Jetson unified memory
    CUDNN_MIX: Set this to 1 to use fp16/mixed precision

Currently, it is working with JETMEM=1 and CUDNN_MIX=1 only.
Also, uncomment ARCH for Jetson (Use compute compute_53)

## Inference weights:
Any existing yolov3 weights, or one that you've trained should  work fine. (How to use: [here](https://pjreddie.com/darknet/yolo/) and [here](https://github.com/AlexeyAB/darknet))

## Training:
I have trained only with cat/dog classes from COCO data set.
I followed the instructions [Training YOLO on COCO](https://pjreddie.com/darknet/yolo/ ) for COCO, but modified to use only these two classes:

* Modify data/coco.names to have only cat and dog classes.
* Generate new train and data set with cat and dog images file only in cfg/coco.data
* Modify cfg/yolov3.cfg to use 2 classes. ([link](https://github.com/AlexeyAB/darknet))

Below are the files of the modifications above (You will have to follow the instructions above to download COCO data set).

    cfg/coco_cat_dog.data 
    data/coco/trainval_cat_dog.txt
    data/coco/val_cat_dog.txt
    data/coco/coco_cat_dog.names
    cfg/yolov3_cat_dog.cfg

also, we need a file with new labels.
perform the following:

    tar xvf coco_cat_dog_labels.tar.gz

WARNING: this will overwrite existing labels to cat and dog only (all other labels will be deleted)

And here is the example to train:

    ./darknet detector train cfg/coco_cat_dog.data cfg/yolov3_cat_dog.cfg weights/darknet53.conv.74 

Again some detailed report of the trainig/inference are [here](https://etanba007.blogspot.com/2020/05/darknetyolo-optimized-for-jetson.html).
