#include "upsample_layer.h"
#include "dark_cuda.h"
#include "blas.h"

#include <stdio.h>

layer make_upsample_layer(int batch, int w, int h, int c, int stride)
{
    layer l = { (LAYER_TYPE)0 };
    l.type = UPSAMPLE;
    l.batch = batch;
    l.w = w;
    l.h = h;
    l.c = c;
    l.out_w = w*stride;
    l.out_h = h*stride;
    l.out_c = c;
    if(stride < 0){
        stride = -stride;
        l.reverse=1;
        l.out_w = w/stride;
        l.out_h = h/stride;
    }
    l.stride = stride;
    l.outputs = l.out_w*l.out_h*l.out_c;
    l.inputs = l.w*l.h*l.c;
#ifndef JETMEM    
#  ifndef CUDNN_MIX    
    l.delta = (float*)calloc(l.outputs * batch, sizeof(float));
    l.output = (float*)calloc(l.outputs * batch, sizeof(float));
#  else
    l.delta16 = (float*)calloc(l.outputs * batch, sizeof(float)/2);
    l.output16 = (float*)calloc(l.outputs * batch, sizeof(float)/2);
#  endif    
#endif
    
    l.forward = forward_upsample_layer;
    l.backward = backward_upsample_layer;
    #ifdef GPU
    l.forward_gpu = forward_upsample_layer_gpu;
    l.backward_gpu = backward_upsample_layer_gpu;

#ifdef JETMEM
#  ifndef CUDNN_MIX    
    cudaMallocManaged(&l.output_gpu, batch*l.outputs*sizeof(float), cudaMemAttachGlobal);
    cudaMallocManaged(&l.delta_gpu , batch*l.outputs*sizeof(float), cudaMemAttachGlobal);	
    l.output = l.output_gpu;
    l.delta  = l.delta_gpu;
    memset(l.output,0,batch*l.outputs*sizeof(float));
    memset(l.delta ,0,batch*l.outputs*sizeof(float));
#  else
    cudaMallocManaged(&l.output_gpu16, batch*l.outputs*sizeof(float)/2, cudaMemAttachGlobal);
    cudaMallocManaged(&l.delta_gpu16 , batch*l.outputs*sizeof(float)/2, cudaMemAttachGlobal);	
    l.output16 = l.output_gpu16;
    l.delta16  = l.delta_gpu16;
    memset(l.output16,0,batch*l.outputs*sizeof(float)/2);
    memset(l.delta16 ,0,batch*l.outputs*sizeof(float)/2);
#  endif    
#else       
#  ifndef CUDNN_MIX    
    l.delta_gpu =  cuda_make_array(l.delta, l.outputs*batch);
    l.output_gpu = cuda_make_array(l.output, l.outputs*batch);
#  else
    l.delta_gpu16 =  cuda_make_array(l.delta16, l.outputs*batch/2);
    l.output_gpu16 = cuda_make_array(l.output16, l.outputs*batch/2);
#  endif    
#endif    
    #endif
    if(l.reverse) fprintf(stderr, "downsample              %2dx  %4d x%4d x%4d -> %4d x%4d x%4d\n", stride, w, h, c, l.out_w, l.out_h, l.out_c);
    else fprintf(stderr, "upsample                %2dx  %4d x%4d x%4d -> %4d x%4d x%4d\n", stride, w, h, c, l.out_w, l.out_h, l.out_c);
    return l;
}

void resize_upsample_layer(layer *l, int w, int h)
{
    l->w = w;
    l->h = h;
    l->out_w = w*l->stride;
    l->out_h = h*l->stride;
    if(l->reverse){
        l->out_w = w/l->stride;
        l->out_h = h/l->stride;
    }
    l->outputs = l->out_w*l->out_h*l->out_c;
    l->inputs = l->h*l->w*l->c;
#ifndef JETMEM    
#  ifndef CUDNN_MIX    
    l->delta = (float*)realloc(l->delta, l->outputs * l->batch * sizeof(float));
    l->output = (float*)realloc(l->output, l->outputs * l->batch * sizeof(float));
#  else
    l->delta16 = (float*)realloc(l->delta16, l->outputs * l->batch * sizeof(float)/2);
    l->output16 = (float*)realloc(l->output16, l->outputs * l->batch * sizeof(float)/2);
#  endif    
#endif
    
#ifdef GPU
#ifdef JETMEM
#  ifndef CUDNN_MIX    
    cudaFree(l->output_gpu);
    cudaFree(l->delta_gpu);
    cudaMallocManaged(&l->output_gpu, l->outputs*l->batch*sizeof(float), cudaMemAttachGlobal);
    cudaMallocManaged(&l->delta_gpu , l->outputs*l->batch*sizeof(float), cudaMemAttachGlobal);	
    l->output = l->output_gpu;
    l->delta  = l->delta_gpu;
    memset(l->output,0,l->outputs*l->batch*sizeof(float));
    memset(l->delta ,0,l->outputs*l->batch*sizeof(float));
#  else
    cudaFree(l->output_gpu16);
    cudaFree(l->delta_gpu16);
    cudaMallocManaged(&l->output_gpu16, l->outputs*l->batch*sizeof(float)/2, cudaMemAttachGlobal);
    cudaMallocManaged(&l->delta_gpu16 , l->outputs*l->batch*sizeof(float)/2, cudaMemAttachGlobal);	
    l->output16 = l->output_gpu16;
    l->delta16  = l->delta_gpu16;
    memset(l->output16,0,l->outputs*l->batch*sizeof(float)/2);
    memset(l->delta16 ,0,l->outputs*l->batch*sizeof(float)/2);
#  endif    
#else           
#  ifndef CUDNN_MIX    
    cuda_free(l->output_gpu);
    cuda_free(l->delta_gpu);
    l->output_gpu  = cuda_make_array(l->output, l->outputs*l->batch);
    l->delta_gpu   = cuda_make_array(l->delta,  l->outputs*l->batch);
#  else
    cuda_free(l->output_gpu16);
    cuda_free(l->delta_gpu16);
    l->output_gpu16  = cuda_make_array(l->output16, l->outputs*l->batch/2);
    l->delta_gpu16   = cuda_make_array(l->delta16,  l->outputs*l->batch/2);
#  endif    
#endif
#endif

}

void forward_upsample_layer(const layer l, network_state net)
{
    fill_cpu(l.outputs*l.batch, 0, l.output, 1);
    if(l.reverse){
        upsample_cpu(l.output, l.out_w, l.out_h, l.c, l.batch, l.stride, 0, l.scale, net.input);
    }else{
        upsample_cpu(net.input, l.w, l.h, l.c, l.batch, l.stride, 1, l.scale, l.output);
    }
}

void backward_upsample_layer(const layer l, network_state state)
{
    if(l.reverse){
        upsample_cpu(l.delta, l.out_w, l.out_h, l.c, l.batch, l.stride, 1, l.scale, state.delta);
    }else{
        upsample_cpu(state.delta, l.w, l.h, l.c, l.batch, l.stride, 0, l.scale, l.delta);
    }
}

#ifdef GPU
void forward_upsample_layer_gpu(const layer l, network_state state)
{
#ifndef CUDNN_MIX  
    fill_ongpu(l.outputs*l.batch, 0, l.output_gpu, 1);
    if(l.reverse){
        upsample_gpu(l.output_gpu, l.out_w, l.out_h, l.c, l.batch, l.stride, 0, l.scale, state.input);
    }else{
        upsample_gpu(state.input, l.w, l.h, l.c, l.batch, l.stride, 1, l.scale, l.output_gpu);
    }
#else
    fill_ongpu16(l.outputs*l.batch, 0, l.output_gpu16, 1);
    if(l.reverse){
      upsample_gpu16(l.output_gpu16, l.out_w, l.out_h, l.c, l.batch, l.stride, 0, l.scale, state.input);
    }else{
      upsample_gpu16(state.input, l.w, l.h, l.c, l.batch, l.stride, 1, l.scale, l.output_gpu16);
    }
#endif    

}

void backward_upsample_layer_gpu(const layer l, network_state state)
{
#ifndef CUDNN_MIX  
    if(l.reverse){
        upsample_gpu(l.delta_gpu, l.out_w, l.out_h, l.c, l.batch, l.stride, 1, l.scale, state.delta);
    }else{
        upsample_gpu(state.delta, l.w, l.h, l.c, l.batch, l.stride, 0, l.scale, l.delta_gpu);
    }
#else
    if(l.reverse){
        upsample_gpu16(l.delta_gpu16, l.out_w, l.out_h, l.c, l.batch, l.stride, 1, l.scale, state.delta);
    }else{        
        upsample_gpu16(state.delta, l.w, l.h, l.c, l.batch, l.stride, 0, l.scale, l.delta_gpu16);
    }
#endif    
}
#endif
