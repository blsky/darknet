#include "cost_layer.h"
#include "utils.h"
#include "dark_cuda.h"
#include "blas.h"
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

COST_TYPE get_cost_type(char *s)
{
    if (strcmp(s, "sse")==0) return SSE;
    if (strcmp(s, "masked")==0) return MASKED;
    if (strcmp(s, "smooth")==0) return SMOOTH;
    fprintf(stderr, "Couldn't find cost type %s, going with SSE\n", s);
    return SSE;
}

char *get_cost_string(COST_TYPE a)
{
    switch(a){
        case SSE:
            return "sse";
        case MASKED:
            return "masked";
        case SMOOTH:
            return "smooth";
		default:
			return "sse";
    }
}

cost_layer make_cost_layer(int batch, int inputs, COST_TYPE cost_type, float scale)
{
    fprintf(stderr, "cost                                           %4d\n",  inputs);
    cost_layer l = { (LAYER_TYPE)0 };
    l.type = COST;

    l.scale = scale;
    l.batch = batch;
    l.inputs = inputs;
    l.outputs = inputs;
    l.cost_type = cost_type;
#ifndef JETMEM
    l.delta = (float*)calloc(inputs * batch, sizeof(float));
    l.output = (float*)calloc(inputs * batch, sizeof(float));
#  ifdef CUDNN_MIX    
    l.delta16 = (float*)calloc(inputs * batch, sizeof(float)/2);
    l.output16 = (float*)calloc(inputs * batch, sizeof(float)/2);
#  endif    
#endif

    l.cost = (float*)calloc(1, sizeof(float));

    l.forward = forward_cost_layer;
    l.backward = backward_cost_layer;
    #ifdef GPU
    l.forward_gpu = forward_cost_layer_gpu;
    l.backward_gpu = backward_cost_layer_gpu;

#ifdef JETMEM
    cudaMallocManaged(&l.output_gpu, batch*inputs*sizeof(float), cudaMemAttachGlobal);
    cudaMallocManaged(&l.delta_gpu , batch*inputs*sizeof(float), cudaMemAttachGlobal);	
    l.output = l.output_gpu;
    l.delta  = l.delta_gpu;
    memset(l.output,0,batch*inputs*sizeof(float));
    memset(l.delta ,0,batch*inputs*sizeof(float));
#  ifdef CUDNN_MIX
    cudaMallocManaged(&l.output_gpu16, batch*inputs*sizeof(float)/2, cudaMemAttachGlobal);
    cudaMallocManaged(&l.delta_gpu16 , batch*inputs*sizeof(float)/2, cudaMemAttachGlobal);	
    l.output16 = l.output_gpu16;
    l.delta16  = l.delta_gpu16;
    memset(l.output16,0,batch*inputs*sizeof(float)/2);
    memset(l.delta16 ,0,batch*inputs*sizeof(float)/2);
#  endif
#else       
    l.delta_gpu =  cuda_make_array(l.delta, inputs*batch);
    l.output_gpu = cuda_make_array(l.output, inputs*batch);
#  ifdef CUDNN_MIX
    l.delta_gpu16 =  cuda_make_array(l.delta16, inputs*batch/2);
    l.output_gpu16 = cuda_make_array(l.output16, inputs*batch/2);
#  endif
#endif    


    #endif
    return l;
}

void resize_cost_layer(cost_layer *l, int inputs)
{
    l->inputs = inputs;
    l->outputs = inputs;
#ifndef JETMEM
    l->delta = (float*)realloc(l->delta, inputs * l->batch * sizeof(float));
    l->output = (float*)realloc(l->output, inputs * l->batch * sizeof(float));
#  ifdef CUDNN_MIX    
    l->delta16 = (float*)realloc(l->delta16, inputs * l->batch * sizeof(float)/2);
    l->output16 = (float*)realloc(l->output16, inputs * l->batch * sizeof(float)/2);
#  endif
#endif


#ifdef GPU
#ifdef JETMEM
    cuda_free(l->output_gpu);
    cuda_free(l->delta_gpu);
    cudaMallocManaged(&l->output_gpu, inputs*l->batch*sizeof(float), cudaMemAttachGlobal);
    cudaMallocManaged(&l->delta_gpu , inputs*l->batch*sizeof(float), cudaMemAttachGlobal);	
    l->output = l->output_gpu;
    l->delta  = l->delta_gpu;
    memset(l->output,0,inputs*l->batch*sizeof(float));
    memset(l->delta ,0,inputs*l->batch*sizeof(float));
#  ifdef CUDNN_MIX    
    cuda_free(l->output_gpu16);
    cuda_free(l->delta_gpu16);
    cudaMallocManaged(&l->output_gpu16, inputs*l->batch*sizeof(float)/2, cudaMemAttachGlobal);
    cudaMallocManaged(&l->delta_gpu16 , inputs*l->batch*sizeof(float)/2, cudaMemAttachGlobal);	
    l->output16 = l->output_gpu16;
    l->delta16  = l->delta_gpu16;
    memset(l->output16,0,inputs*l->batch*sizeof(float)/2);
    memset(l->delta16 ,0,inputs*l->batch*sizeof(float)/2);
#  endif
#else       
    l->output_gpu  = cuda_make_array(l->output, inputs*l->batch);
    l->delta_gpu   = cuda_make_array(l->delta,  inputs*l->batch);
#  ifdef CUDNN_MIX    
    l->output_gpu16  = cuda_make_array(l->output16, inputs*l->batch/2);
    l->delta_gpu16   = cuda_make_array(l->delta16,  inputs*l->batch/2);
#  endif
#endif

#endif
}

void forward_cost_layer(cost_layer l, network_state state)
{
    if (!state.truth) return;
    if(l.cost_type == MASKED){
        int i;
        for(i = 0; i < l.batch*l.inputs; ++i){
            if(state.truth[i] == SECRET_NUM) state.input[i] = SECRET_NUM;
        }
    }
    if(l.cost_type == SMOOTH){
        smooth_l1_cpu(l.batch*l.inputs, state.input, state.truth, l.delta, l.output);
    } else {
        l2_cpu(l.batch*l.inputs, state.input, state.truth, l.delta, l.output);
    }
    l.cost[0] = sum_array(l.output, l.batch*l.inputs);
}

void backward_cost_layer(const cost_layer l, network_state state)
{
    axpy_cpu(l.batch*l.inputs, l.scale, l.delta, 1, state.delta, 1);
}

#ifdef GPU

void pull_cost_layer(cost_layer l)
{
printf("cost_layer pull ... shall adapt to CUDNN_MIX\n");
exit(-1);
/*
    cuda_pull_array(l.delta_gpu, l.delta, l.batch*l.inputs);
*/
}

void push_cost_layer(cost_layer l)
{
printf("cost_layer push ... shall adapt to CUDNN_MIX\n");
exit(-1);
/*
    cuda_push_array(l.delta_gpu, l.delta, l.batch*l.inputs);
*/
}

int float_abs_compare (const void * a, const void * b)
{
    float fa = *(const float*) a;
    if(fa < 0) fa = -fa;
    float fb = *(const float*) b;
    if(fb < 0) fb = -fb;
    return (fa > fb) - (fa < fb);
}

void forward_cost_layer_gpu(cost_layer l, network_state state)
{
    if (!state.truth) {
#ifdef CUDNN_MIX        
      cuda_convert_f16_to_f32(state.input, l.batch*l.inputs,  l.output_gpu);
#  ifndef JETMEM
      cuda_pull_array(l.output_gpu, l.output, l.batch*l.inputs);
#  endif
      cudaDeviceSynchronize();
#endif
      return;
    }
    if (l.cost_type == MASKED) {
printf("cost_layer forward MASKED... shall adapt to CUDNN_MIX\n");
exit(-1);
//        mask_ongpu(l.batch*l.inputs, state.input, SECRET_NUM, state.truth);
    }

    if(l.cost_type == SMOOTH){
printf("cost_layer forward SMOOTH... shall adapt to CUDNN_MIX\n");
exit(-1);
//        smooth_l1_gpu(l.batch*l.inputs, state.input, state.truth, l.delta_gpu, l.output_gpu);
    } else {
#ifndef CUDNN_MIX        
        l2_gpu(l.batch*l.inputs, state.input, state.truth, l.delta_gpu, l.output_gpu);
#else
        l2_gpu_mix(l.batch*l.inputs, state.input, state.truth, l.delta_gpu, l.output_gpu);
#endif
    }

    if(l.ratio){
printf("cost_layer forward ratio... shall adapt to CUDNN_MIX\n");
exit(-1);
/*
        cuda_pull_array(l.delta_gpu, l.delta, l.batch*l.inputs);
        qsort(l.delta, l.batch*l.inputs, sizeof(float), float_abs_compare);
        int n = (1-l.ratio) * l.batch*l.inputs;
        float thresh = l.delta[n];
        thresh = 0;
        printf("%f\n", thresh);
        supp_ongpu(l.batch*l.inputs, thresh, l.delta_gpu, 1);
*/
    }

#ifndef JETMEM
    cuda_pull_array(l.output_gpu, l.output, l.batch*l.inputs);
#endif
    cudaDeviceSynchronize();
    l.cost[0] = sum_array(l.output, l.batch*l.inputs);
#ifdef CUDNN_MIX
    scale_loss_gpu_mix(l.delta_gpu, *state.net.loss_scale, l.delta_gpu16, l.batch*l.inputs);
#endif


}

void backward_cost_layer_gpu(const cost_layer l, network_state state)
{

#ifndef CUDNN_MIX
    axpy_ongpu(l.batch*l.inputs, l.scale, l.delta_gpu, 1, state.delta, 1);
#else
    axpy_ongpu16(l.batch*l.inputs, l.scale, l.delta_gpu16, 1, state.delta, 1);
#endif    

}
#endif
