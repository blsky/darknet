#include "route_layer.h"
#include "dark_cuda.h"
#include "blas.h"
#include <stdio.h>

route_layer make_route_layer(int batch, int n, int *input_layers, int *input_sizes, int groups, int group_id)
{
    fprintf(stderr,"route ");
    route_layer l = { (LAYER_TYPE)0 };
    l.type = ROUTE;
    l.batch = batch;
    l.n = n;
    l.input_layers = input_layers;
    l.input_sizes = input_sizes;
    l.groups = groups;
    l.group_id = group_id;
    int i;
    int outputs = 0;
    for(i = 0; i < n; ++i){
        fprintf(stderr," %d", input_layers[i]);
        outputs += input_sizes[i];
    }
    outputs = outputs / groups;
    l.outputs = outputs;
    l.inputs = outputs;
#ifndef JETMEM
#  ifndef CUDNN_MIX    
    //fprintf(stderr, " inputs = %d \t outputs = %d, groups = %d, group_id = %d \n", l.inputs, l.outputs, l.groups, l.group_id);
    l.delta = (float*)calloc(outputs * batch, sizeof(float));
    l.output = (float*)calloc(outputs * batch, sizeof(float));
#  else
    l.delta16 = (float*)calloc(outputs * batch, sizeof(float)/2);
    l.output16 = (float*)calloc(outputs * batch, sizeof(float)/2);
#  endif    
#endif
    
    l.forward = forward_route_layer;
    l.backward = backward_route_layer;
    #ifdef GPU
    l.forward_gpu = forward_route_layer_gpu;
    l.backward_gpu = backward_route_layer_gpu;

#ifdef JETMEM
#  ifndef CUDNN_MIX
    cudaMallocManaged(&l.output_gpu, batch*outputs*sizeof(float), cudaMemAttachGlobal);
    cudaMallocManaged(&l.delta_gpu , batch*outputs*sizeof(float), cudaMemAttachGlobal);	
    l.output = l.output_gpu;
    l.delta  = l.delta_gpu;
    memset(l.output,0,batch*outputs*sizeof(float));
    memset(l.delta ,0,batch*outputs*sizeof(float));
#  else
    cudaMallocManaged(&l.output_gpu16, batch*outputs*sizeof(float)/2, cudaMemAttachGlobal);
    cudaMallocManaged(&l.delta_gpu16 , batch*outputs*sizeof(float)/2, cudaMemAttachGlobal);	
    l.output16 = l.output_gpu16;
    l.delta16  = l.delta_gpu16;
    memset(l.output16,0,batch*outputs*sizeof(float)/2);
    memset(l.delta16 ,0,batch*outputs*sizeof(float)/2);
#  endif
#else       
#  ifndef CUDNN_MIX
    l.delta_gpu =  cuda_make_array(l.delta, outputs*batch);
    l.output_gpu = cuda_make_array(l.output, outputs*batch);
#  else
    l.delta_gpu16 =  cuda_make_array(l.delta16, outputs*batch/2);
    l.output_gpu16 = cuda_make_array(l.output16, outputs*batch/2);
#  endif
#endif    
    #endif
    return l;
}

void resize_route_layer(route_layer *l, network *net)
{
    int i;
    layer first = net->layers[l->input_layers[0]];
    l->out_w = first.out_w;
    l->out_h = first.out_h;
    l->out_c = first.out_c;
    l->outputs = first.outputs;
    l->input_sizes[0] = first.outputs;
    for(i = 1; i < l->n; ++i){
        int index = l->input_layers[i];
        layer next = net->layers[index];
        l->outputs += next.outputs;
        l->input_sizes[i] = next.outputs;
        if(next.out_w == first.out_w && next.out_h == first.out_h){
            l->out_c += next.out_c;
        }else{
            printf("%d %d, %d %d\n", next.out_w, next.out_h, first.out_w, first.out_h);
            l->out_h = l->out_w = l->out_c = 0;
        }
    }
    l->out_c = l->out_c / l->groups;
    l->outputs = l->outputs / l->groups;
    l->inputs = l->outputs;
#ifndef JETMEM
#  ifndef CUDNN_MIX    
    l->delta = (float*)realloc(l->delta, l->outputs * l->batch * sizeof(float));
    l->output = (float*)realloc(l->output, l->outputs * l->batch * sizeof(float));
#  else
    l->delta16 = (float*)realloc(l->delta16, l->outputs * l->batch * sizeof(float)/2);
    l->output16 = (float*)realloc(l->output16, l->outputs * l->batch * sizeof(float)/2);
#  endif
#endif
    
#ifdef GPU

#ifdef JETMEM
#  ifndef CUDNN_MIX    
    cuda_free(l->output_gpu);
    cuda_free(l->delta_gpu);
    cudaMallocManaged(&l->output_gpu, l->outputs*l->batch*sizeof(float), cudaMemAttachGlobal);
    cudaMallocManaged(&l->delta_gpu , l->outputs*l->batch*sizeof(float), cudaMemAttachGlobal);	
    l->output = l->output_gpu;
    l->delta  = l->delta_gpu;
    memset(l->output,0,l->outputs*l->batch*sizeof(float));
    memset(l->delta ,0,l->outputs*l->batch*sizeof(float));
#  else
    cuda_free(l->output_gpu16);
    cuda_free(l->delta_gpu16);
    cudaMallocManaged(&l->output_gpu16, l->outputs*l->batch*sizeof(float)/2, cudaMemAttachGlobal);
    cudaMallocManaged(&l->delta_gpu16 , l->outputs*l->batch*sizeof(float)/2, cudaMemAttachGlobal);	
    l->output16 = l->output_gpu16;
    l->delta16  = l->delta_gpu16;
    memset(l->output16,0,l->outputs*l->batch*sizeof(float)/2);
    memset(l->delta16 ,0,l->outputs*l->batch*sizeof(float)/2);
#  endif
#else       
#  ifndef CUDNN_MIX    
    l->output_gpu  = cuda_make_array(l->output, l->outputs*l->batch);
    l->delta_gpu   = cuda_make_array(l->delta,  l->outputs*l->batch);
#  else
    l->output_gpu16  = cuda_make_array(l->output16, l->outputs*l->batch/2);
    l->delta_gpu16   = cuda_make_array(l->delta16,  l->outputs*l->batch/2);
#  endif
#endif
#endif

}

void forward_route_layer(const route_layer l, network_state state)
{
    int i, j;
    int offset = 0;
    for(i = 0; i < l.n; ++i){
        int index = l.input_layers[i];
        float *input = state.net.layers[index].output;
        int input_size = l.input_sizes[i];
        int part_input_size = input_size / l.groups;
        for(j = 0; j < l.batch; ++j){
            //copy_cpu(input_size, input + j*input_size, 1, l.output + offset + j*l.outputs, 1);
            copy_cpu(part_input_size, input + j*input_size + part_input_size*l.group_id, 1, l.output + offset + j*l.outputs, 1);
        }
        //offset += input_size;
        offset += part_input_size;
    }
}

void backward_route_layer(const route_layer l, network_state state)
{
    int i, j;
    int offset = 0;
    for(i = 0; i < l.n; ++i){
        int index = l.input_layers[i];
        float *delta = state.net.layers[index].delta;
        int input_size = l.input_sizes[i];
        int part_input_size = input_size / l.groups;
        for(j = 0; j < l.batch; ++j){
            //axpy_cpu(input_size, 1, l.delta + offset + j*l.outputs, 1, delta + j*input_size, 1);
            axpy_cpu(part_input_size, 1, l.delta + offset + j*l.outputs, 1, delta + j*input_size + part_input_size*l.group_id, 1);
        }
        //offset += input_size;
        offset += part_input_size;
    }
}

#ifdef GPU
void forward_route_layer_gpu(const route_layer l, network_state state)
{
    int i, j;
    int offset = 0;
    for(i = 0; i < l.n; ++i){
        int index = l.input_layers[i];
#ifndef CUDNN_MIX	
        float *input = state.net.layers[index].output_gpu;
#else
        float *input = state.net.layers[index].output_gpu16;
#endif	
        int input_size = l.input_sizes[i];
        int part_input_size = input_size / l.groups;
        for(j = 0; j < l.batch; ++j){
            //copy_ongpu(input_size, input + j*input_size, 1, l.output_gpu + offset + j*l.outputs, 1);
#ifndef CUDNN_MIX	
            //simple_copy_ongpu(input_size, input + j*input_size, l.output_gpu + offset + j*l.outputs);
            simple_copy_ongpu(part_input_size, input + j*input_size + part_input_size*l.group_id, l.output_gpu + offset + j*l.outputs);
#else
            //simple_copy_ongpu16(input_size, input + j*input_size, l.output_gpu16 + offset + j*l.outputs);
            simple_copy_ongpu16(part_input_size, input + (j*input_size + part_input_size*l.group_id)/2, l.output_gpu16 + (offset + j*l.outputs)/2);
#endif	    
            //simple_copy_ongpu(input_size, input + j*input_size, l.output_gpu + offset + j*l.outputs);
        }
        //offset += input_size;
        offset += part_input_size;
    }
}

void backward_route_layer_gpu(const route_layer l, network_state state)
{
    int i, j;
    int offset = 0;
    for(i = 0; i < l.n; ++i){
        int index = l.input_layers[i];
#ifndef CUDNN_MIX
        float *delta = state.net.layers[index].delta_gpu;
        int input_size = l.input_sizes[i];
        int part_input_size = input_size / l.groups;
        for(j = 0; j < l.batch; ++j){
            //axpy_ongpu(input_size, 1, l.delta_gpu + offset + j*l.outputs, 1, delta + j*input_size, 1);
            axpy_ongpu(part_input_size, 1, l.delta_gpu + offset + j*l.outputs, 1, delta + j*input_size + part_input_size*l.group_id, 1);
        }
#else
        float *delta16 = state.net.layers[index].delta_gpu16;
        int input_size = l.input_sizes[i];
        int part_input_size = input_size / l.groups;
        for(j = 0; j < l.batch; ++j){
            axpy_ongpu16(input_size, 1, l.delta_gpu16 + (offset + j*l.outputs)/2, 1, delta16 + (j*input_size)/2, 1);
        }
#endif	
        //offset += input_size;
        offset += part_input_size;
    }
}
#endif
