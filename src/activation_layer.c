#include "activation_layer.h"
#include "utils.h"
#include "dark_cuda.h"
#include "blas.h"
#include "gemm.h"

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

layer make_activation_layer(int batch, int inputs, ACTIVATION activation)
{
    layer l = { (LAYER_TYPE)0 };
    l.type = ACTIVE;

    l.inputs = inputs;
    l.outputs = inputs;
    l.batch=batch;

    l.output = (float*)calloc(batch * inputs, sizeof(float));
    l.delta = (float*)calloc(batch * inputs, sizeof(float));

    l.forward = forward_activation_layer;
    l.backward = backward_activation_layer;
#ifdef GPU
    l.forward_gpu = forward_activation_layer_gpu;
    l.backward_gpu = backward_activation_layer_gpu;

    l.output_gpu = cuda_make_array(l.output, inputs*batch);
#  ifndef CUDNN_MIX
    l.delta_gpu = cuda_make_array(l.delta, inputs*batch);
#  else
    l.delta_gpu16 = cuda_make_array(l.delta16, inputs*batch/2);
#  endif
#endif
    l.activation = activation;
    fprintf(stderr, "Activation Layer: %d inputs\n", inputs);
    return l;
}

void forward_activation_layer(layer l, network_state state)
{
    copy_cpu(l.outputs*l.batch, state.input, 1, l.output, 1);
    activate_array(l.output, l.outputs*l.batch, l.activation);
}

void backward_activation_layer(layer l, network_state state)
{
    gradient_array(l.output, l.outputs*l.batch, l.activation, l.delta);
    copy_cpu(l.outputs*l.batch, l.delta, 1, state.delta, 1);
}

#ifdef GPU

void forward_activation_layer_gpu(layer l, network_state state)
{
#ifndef CUDNN_MIX  
    copy_ongpu(l.outputs*l.batch, state.input, 1, l.output_gpu, 1);
    activate_array_ongpu(l.output_gpu, l.outputs*l.batch, l.activation);
#else
    copy_ongpu16(l.outputs*l.batch, state.input, 1, l.output_gpu16, 1);
    activate_array_ongpu16(l.output_gpu16, l.outputs*l.batch, l.activation);
#endif    
}

void backward_activation_layer_gpu(layer l, network_state state)
{
#  ifndef CUDNN_MIX
    gradient_array_ongpu(l.output_gpu, l.outputs*l.batch, l.activation, l.delta_gpu);
    copy_ongpu(l.outputs*l.batch, l.delta_gpu, 1, state.delta, 1);
#  else
    gradient_array_ongpu16(l.output_gpu16, l.outputs*l.batch, l.activation, l.delta_gpu16);
    copy_ongpu16(l.outputs*l.batch, l.delta_gpu16, 1, state.delta, 1);
#  endif
}
#endif
